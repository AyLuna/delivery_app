import 'package:easy_localization/easy_localization.dart';

import '../authentication/login.dart';
import '../authentication/register.dart';
import 'package:flutter/material.dart';

import '../generated/locale_keys.g.dart';
import '../widgets/styled_text_widget.dart';


class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Color.fromRGBO(241, 241, 241, 1),
            child: Column(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children:[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(13, 10, 10, 8),
                      child: StyledTextWidget(text:LocaleKeys.Sign_Up.tr(),size: 24,spacing: 2,weight: FontWeight.w500,),
                    ),
                  ],
                ),
                SizedBox(height: 9,),
                RegisterScreen(),
                SizedBox(height: 20,),
                TextButton(onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (c)=>LoginScreen()));
                }, child: Center(
                  child: Column(
                    children: [
                      StyledTextWidget(text:LocaleKeys.I_have_an_account_already.tr()),
                      StyledTextWidget(text:LocaleKeys.Sign_In.tr(),color: Colors.blue,),
                    ],
                  ),
                ),)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
