import 'package:cloud_firestore/cloud_firestore.dart';
import '../global/global.dart';


seperateOrderItemQuantities(orderIDs){
  List<String> seperateItemQuantitiesList=[];
  List<String> defaultItemList=[];
  int i=1;

  defaultItemList=List<String>.from(orderIDs);

  for(i; i<defaultItemList.length; i++){
    //56557657:7
    String item=defaultItemList[i].toString();


                                                 //56557657  :   7 " ':'sundan ika bol we liste owur"     List listItemCharacters=[56557657, 7]
                                                //listItemCharacters[0]=56557657
                                                //listItemCharacters[1]=7
    //7
    List<String> listItemCharacters=item.split(":").toList();

    var quantityNumber=int.parse(listItemCharacters[1].toString());


    print("\n This is quantity number"+quantityNumber.toString());

    seperateItemQuantitiesList.add(quantityNumber.toString());
  }
  print("\n This is quantity number list now");
  print(seperateItemQuantitiesList);

  return seperateItemQuantitiesList;

}

seperateOrderItemIDs(orderIDs){
  List<String> seperateItemIDsList=[], defaultItemList=[];
  int i=0;

  defaultItemList=List<String>.from(orderIDs);

  for(i; i<defaultItemList.length; i++){

    //56557657:7
    String item=defaultItemList[i].toString();

    //56557657
    var pos=item.lastIndexOf(":");
    String getItemId=(pos!=-1)?item.substring(0, pos):item;

    print("\n This is itemID now"+getItemId);

    seperateItemIDsList.add(getItemId);
  }
  print("\n This is items list now");
  print(seperateItemIDsList);

  return seperateItemIDsList;
}


seperateItemQuantities(){
  List<int> seperateItemQuantitiesList=[];
  List<String> defaultItemList=[];
  int i=1;

  defaultItemList=sharedPreferences!.getStringList("userCart")!;

  for(i; i<defaultItemList.length; i++){
    //56557657:7
    String item=defaultItemList[i].toString();


    //56557657  :   7 " ':'sundan ika bol we liste owur"     List listItemCharacters=[56557657, 7]
    //listItemCharacters[0]=56557657
    //listItemCharacters[1]=7
    //7
    List<String> listItemCharacters=item.split(":").toList();

    var quantityNumber=int.parse(listItemCharacters[1].toString());


    print("\n This is quantity number"+quantityNumber.toString());

    seperateItemQuantitiesList.add(quantityNumber);
  }
  print("\n This is quantity number list now");
  print(seperateItemQuantitiesList);

  return seperateItemQuantitiesList;

}

seperateItemIDs(){
  List<String> seperateItemIDsList=[], defaultItemList=[];
  int i=0;

  defaultItemList=sharedPreferences!.getStringList("userCart")!;

  for(i; i<defaultItemList.length; i++){

    //56557657:7
    String item=defaultItemList[i].toString();

    //56557657
    var pos=item.lastIndexOf(":");
    String getItemId=(pos!=-1)?item.substring(0, pos):item;

    print("\n This is itemID now"+getItemId);

    seperateItemIDsList.add(getItemId);
  }
  print("\n This is items list now");
  print(seperateItemIDsList);

  return seperateItemIDsList;
}


clearCartNow(context){
  sharedPreferences!.setStringList("userCart", ['garbagevalue']);
  List<String>? emptyList=sharedPreferences!.getStringList("userCart");


  FirebaseFirestore.instance.collection("users").doc(firebaseAuth.currentUser!.uid).update({"userCart":emptyList}).then((value){
    sharedPreferences!.setStringList("userCart", emptyList!);
  });
}