
import 'constants.dart';

class Endpoints {
  String get login => '$url/login';

  String products(int page) => '$url/auth/products?page=$page';

  String get createProduct => '$url/auth/product';

  String get categories => '$url/auth/categories';

  String deleteProduct(id) => '$url/auth/product/$id';

  String get updateProfile => '$url/auth/update';
}
