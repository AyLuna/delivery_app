import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HttpClient {
  Future<dynamic> get(
    String url, {
    Map<String, String>? headers,
  }) async {
    var response;
    final header = <String, String>{
      'Accept': 'application/json',
    }..addAll(headers ?? {});
    var client = http.Client();
    try {
      response = await client.get(
        Uri.parse(url),
        headers: header,
      );
    } on HttpException {
      print(HttpException);
    }
    return json.decode(response.body);
  }

  Future<dynamic> post(
    String url, {
    Map<String, String>? headers,
    Map<String, dynamic>? data,
    XFile? file,
  }) async {
    final header = <String, String>{
      'Accept': 'application/json',
    }..addAll(headers ?? {});
    try {
      var response;
      var request = await http.MultipartRequest('POST', Uri.parse(url));
      header.forEach((key, value) {
        request.headers[key] = value;
      });
      data!.forEach((key, value) {
        request.fields[key] = value;
      });
      if (file != null) {
        request.files
            .add(await http.MultipartFile.fromPath("image", file.path));
      }
      response = await request.send();
      final respStr = await response.stream.bytesToString();
      final prefs = await SharedPreferences.getInstance();
      if (json.decode(respStr)['message'] == 'Unauthenticated.') {
        prefs.remove('token');
      }
      return json.decode(respStr);
    } on HttpException {
      print(HttpException);
    }
  }

  Future<dynamic> delete(
    String url, {
    Map<String, String>? headers,
    Map<String, dynamic>? data,
  }) async {
    var response;
    final header = <String, String>{
      'Accept': 'application/json',
    }..addAll(headers ?? {});
    var client = http.Client();
    try {
      response = await client.delete(
        Uri.parse(url),
        headers: header,
        body: data,
      );
    } on HttpException {
      print(HttpException);
    }
    return json.decode(response.body);
  }
}
