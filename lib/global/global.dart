
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences? sharedPreferences;
FirebaseAuth firebaseAuth=FirebaseAuth.instance;

Position? position;
List<Placemark>? placeMarks;

String completeAdress="";


String perParcelDeliveryAmount="";
String previousSellerEarnings = ""; //it is seller old total earnings
String previousRiderEarnings = ""; //it is rider old total earnings


String defaultDil=sharedPreferences!.getString("language")==null?"en":sharedPreferences!.getString("language")!;

