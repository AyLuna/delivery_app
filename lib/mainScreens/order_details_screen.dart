import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dastawka/widgets/styled_text_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../modal/adress.dart';
import '../widgets/progress_bar.dart';
import '../widgets/shipment_address_design.dart';
import '../widgets/status_banner.dart';
import 'package:intl/intl.dart';


class OrderDetailsScreen extends StatefulWidget
{
  final String? orderID;

  OrderDetailsScreen({this.orderID});

  @override
  _OrderDetailsScreenState createState() => _OrderDetailsScreenState();
}




class _OrderDetailsScreenState extends State<OrderDetailsScreen>
{
  String orderStatus = "";
  String orderByUser = "";
  String sellerId = "";

  getOrderInfo()
  {
    FirebaseFirestore.instance
        .collection("orders")
        .doc(widget.orderID).get().then((DocumentSnapshot)
    {
      orderStatus = DocumentSnapshot.data()!["status"].toString();
      orderByUser = DocumentSnapshot.data()!["orderBy"].toString();
      sellerId = DocumentSnapshot.data()!["sellerUID"].toString();
    });
  }

  @override
  void initState() {
    super.initState();

    getOrderInfo();
  }

  @override
  Widget build(BuildContext context)
  {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
          body: SingleChildScrollView(
            child: FutureBuilder<DocumentSnapshot>(
              future: FirebaseFirestore.instance
                  .collection("orders")
                  .doc(widget.orderID)
                  .get(),
              builder: (c, snapshot)
              {
                Map? dataMap;
                if(snapshot.hasData)
                {
                  dataMap = snapshot.data!.data()! as Map<String, dynamic>;
                  orderStatus = dataMap["status"].toString();
                }
                return snapshot.hasData
                    ? Container(
                        child: Column(
                          children: [
                            StatusBanner(
                              status: dataMap!["isSuccess"],
                              orderStatus: orderStatus,
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: StyledTextWidget(text: dataMap["totalAmount"].toString()+LocaleKeys.manat.tr(), size:24, weight: FontWeight.bold,),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: StyledTextWidget(text: LocaleKeys.Order_Id.tr() + widget.orderID!),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: StyledTextWidget(text: LocaleKeys.Order_at.tr() +
                                  DateFormat("dd MMMM, yyy - hh:mm aa")
                                      .format(DateTime.fromMillisecondsSinceEpoch(int.parse(dataMap["orderTime"]))), color: Colors.grey),
                              ),
                            orderStatus == "ended"
                                ? Container(
                                width:MediaQuery.of(context).size.width-20,
                                height: MediaQuery.of(context).size.height*0.35,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  image: DecorationImage(image: AssetImage("images/success.jpg"),fit: BoxFit.cover),),)
                                : Container(
                                width:MediaQuery.of(context).size.width-20,
                                height: MediaQuery.of(context).size.height*0.35,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  image: DecorationImage(image: AssetImage("images/confirm_pick.jpg"),fit: BoxFit.cover),),),
                            FutureBuilder<DocumentSnapshot>(
                              future: FirebaseFirestore.instance
                                  .collection("users")
                                  .doc(orderByUser)
                                  .collection("userAdress")
                                  .doc(dataMap["adressId"])
                                  .get(),
                              builder: (c, snapshot)
                              {
                                return snapshot.hasData
                                    ? ShipmentAddressDesign(
                                        model: Adress.fromJson(
                                          snapshot.data!.data()! as Map<String, dynamic>
                                        ),
                                        orderStatus: orderStatus,
                                        orderId: widget.orderID,
                                        sellerId: sellerId,
                                        orderByUser: orderByUser,
                                      )
                                    : Center(child: circularProgress(),);
                              },
                            ),
                          ],
                        ),
                      )
                    : Center(child: circularProgress(),);
              },
            ),
          ),
        ),
      ),
    );
  }
}
