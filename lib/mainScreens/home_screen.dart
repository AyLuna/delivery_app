import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/earnings_screen.dart';
import '../mainScreens/history_screen.dart';
import '../assistendMethods/get_current_location.dart';
import '../authentication/auth_screen.dart';
import '../global/global.dart';
import '../mainScreens/new_orders_screen.dart';
import '../mainScreens/parcel_in_progress_screen.dart';
import '../widgets/home_screen_app_bar.dart';
import '../widgets/styled_text_widget.dart';
import 'not_yet_delivered_screen.dart';


class HomeScreen extends StatefulWidget{
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}



class _HomeScreenState extends State<HomeScreen>
{
  makeDashboardItem(String title, IconData iconData, int index)
  {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          width:MediaQuery.of(context).size.width*0.5-20,
              height: MediaQuery.of(context).size.height*0.2,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Color.fromRGBO(240, 86, 52, 1)),),
          child: InkWell(
            onTap: ()
            {
              if(index == 0)
              {
                //New Available Orders
                Navigator.push(context, MaterialPageRoute(builder: (c)=> NewOrdersScreen()));
              }
              if(index == 1)
              {
                //Parcels in Progress
                Navigator.push(context, MaterialPageRoute(builder: (c)=> ParcelInProgressScreen()));
              }
              if(index == 2)
              {
                //Not Yet Delivered
                Navigator.push(context, MaterialPageRoute(builder: (c)=> NotYetDeliveredScreen()));
              }
              if(index == 3)
              {
                //History
                Navigator.push(context, MaterialPageRoute(builder: (c)=> HistoryScreen()));
              }
              if(index == 4)
              {
                //Total Earnings
                Navigator.push(context, MaterialPageRoute(builder: (c)=> EarningsScreen()));
              }
              if(index == 5)
              {
                //Logout
                firebaseAuth.signOut().then((value){
                  Navigator.push(context, MaterialPageRoute(builder: (c)=> const AuthScreen()));
                });
              }
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Icon(
                    iconData,
                    size: 40,
                    color: Color.fromRGBO(240, 86, 52, 1),
                  ),
                ),
                const SizedBox(height: 10.0),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: StyledTextWidget(text: title,size: 16,weight:FontWeight.w500,color: Color.fromRGBO(240, 86, 52, 1),maxLines: 3, spacing: 0,),
                ),
              ],
            ),
          ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    RiderLocation rLocation = RiderLocation();
    rLocation.getCurrentLocation();

    getPerParcelDeliveryAmount();
    getRiderPreviousEarnings();
    getLangugeValue();
  }

  getLangugeValue(){
    defaultDil=sharedPreferences!.getString("language")==null?"en":sharedPreferences!.getString("language")!;

  }

  getRiderPreviousEarnings()
  {
    FirebaseFirestore.instance
        .collection("riders")
        .doc(sharedPreferences!.getString("uid"))
        .get().then((snap)
    {
      previousRiderEarnings = snap.data()!["earnings"].toString();//variable globalda
    });
  }


  getPerParcelDeliveryAmount()
  {
    FirebaseFirestore.instance
        .collection("dastawkaBaha")
        .doc("6Djc9M0EaXXjjmlufoiO")
        .get().then((snap)
    {
      perParcelDeliveryAmount = snap.data()!["baha"].toString();//variable globalda
    });
  }



  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: HomeScreenAppBar(),
        body: Container(
          padding: const EdgeInsets.symmetric(vertical: 25),
          child: GridView.count(
            crossAxisCount: 2,
            padding: const EdgeInsets.all(2),
            children: [
              makeDashboardItem(LocaleKeys.New_Orders.tr(), Icons.assignment, 0),
              makeDashboardItem(LocaleKeys.Picked_Orders.tr(), Icons.airport_shuttle, 1),
              makeDashboardItem(LocaleKeys.Not_Yet_Delivered.tr(), Icons.location_history, 2),
              makeDashboardItem(LocaleKeys.Orders_History.tr(), Icons.done_all, 3),
              makeDashboardItem(LocaleKeys.Total_Earnings.tr(), Icons.monetization_on, 4),
              makeDashboardItem(LocaleKeys.Logout.tr(), Icons.logout, 5),
            ],
          ),
        ),
      ),
    );
  }
}
