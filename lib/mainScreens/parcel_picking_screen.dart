import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../assistendMethods/get_current_location.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../mainScreens/parcel_delivering_screen.dart';
import '../maps/map_utils.dart';
import '../widgets/simple_app_bar.dart';
import '../widgets/styled_text_widget.dart';

class ParcelPickingScreen extends StatefulWidget
{
  String? purchaserId;
  String? sellerId;
  String? getOrderId;
  String? purchaserAdress;
  double? purchaserLat;
  double? purchaserLng;

  ParcelPickingScreen({
    this.purchaserId,
    this.sellerId,
    this.getOrderId,
    this.purchaserAdress,
    this.purchaserLat,
    this.purchaserLng,
  });

  @override
  _ParcelPickingScreenState createState() => _ParcelPickingScreenState();
}



class _ParcelPickingScreenState extends State<ParcelPickingScreen>
{
  double? sellerLat, sellerLng;

  getSellerData() async
  {
    FirebaseFirestore.instance
        .collection("sellers")
        .doc(widget.sellerId)
        .get()
        .then((DocumentSnapshot)
    {
         sellerLat = DocumentSnapshot.data()!["lat"];
         sellerLng = DocumentSnapshot.data()!["lng"];
    });
  }

  @override
  void initState() {
    super.initState();

    getSellerData();
  }

  confirmParcelHasBeenPicked(getOrderId, sellerId, purchaserId, purchaserAdress, purchaserLat, purchaserLng)
  {
    FirebaseFirestore.instance
        .collection("orders")
        .doc(getOrderId).update({
      "status": "delivering",
      "adress": completeAdress,
      "lat": position!.latitude,
      "lng": position!.longitude,
    });

    Navigator.push(context, MaterialPageRoute(builder: (c)=> ParcelDeliveringScreen(
      purchaserId:purchaserId,
      purchaserAdress: purchaserAdress,
      purchaserLat: purchaserLat,
      purchaserLng: purchaserLng,
      sellerId: sellerId,
      getOrderId: getOrderId,

    )));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
          appBar: SimpleAppBar(title: LocaleKeys.Parcel_is_being_picked.tr(),),
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 30, 10, 0),
                child: Container(
                    width:MediaQuery.of(context).size.width-20,
                    height: MediaQuery.of(context).size.height*0.35,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage("images/confirm1.jpg"),fit: BoxFit.cover),
                      border: Border.all(color: Colors.grey),),),
              ),
              const SizedBox(height: 5,),

              GestureDetector(
                onTap: ()
                {
                  //show location from rider current location towards seller location
                  MapUtils.lauchMapFromSourceToDestination(position!.latitude, position!.longitude, sellerLat, sellerLng);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'images/restaurant.png',
                      width: 50,
                    ),
                    const SizedBox(width: 7,),
                    Column(
                      children:  [
                        SizedBox(height: 12,),
                        StyledTextWidget(text: LocaleKeys.Show_Sellers_Location.tr(),maxLines: 3,),
                      ],
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                child: Center(
                  child: InkWell(
                    onTap: ()
                    {
                      RiderLocation rLocation = RiderLocation();
                      rLocation.getCurrentLocation();

                      //confirmed - that rider has picked parcel from seller
                      confirmParcelHasBeenPicked(
                          widget.getOrderId,
                          widget.sellerId,
                          widget.purchaserId,
                          widget.purchaserAdress,
                          widget.purchaserLat,
                          widget.purchaserLng

                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(240, 86, 52, 1),
                        borderRadius: BorderRadius.circular(20),),
                      width: MediaQuery.of(context).size.width - 70,
                      height: 70,
                      child: Center(
                        child: StyledTextWidget(text: LocaleKeys.Order_has_been_picked.tr(),size: 15,color: Colors.white,maxLines: 3,),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
