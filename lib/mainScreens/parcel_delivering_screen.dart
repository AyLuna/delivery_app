import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../assistendMethods/get_current_location.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../maps/map_utils.dart';
import '../widgets/simple_app_bar.dart';
import '../widgets/styled_text_widget.dart';
import 'home_screen.dart';


class ParcelDeliveringScreen extends StatefulWidget
{
  String? purchaserId;
  String? purchaserAdress;
  double? purchaserLat;
  double? purchaserLng;
  String? sellerId;
  String? getOrderId;

  ParcelDeliveringScreen({
    this.purchaserId,
    this.purchaserAdress,
    this.purchaserLat,
    this.purchaserLng,
    this.sellerId,
    this.getOrderId,
  });


  @override
  _ParcelDeliveringScreenState createState() => _ParcelDeliveringScreenState();
}




class _ParcelDeliveringScreenState extends State<ParcelDeliveringScreen>
{
  String orderTotalAmount = "";


  confirmParcelHasBeenDelivered(getOrderId, sellerId, purchaserId, purchaserAdress, purchaserLat, purchaserLng)
  {

    String riderNewTotalEarningAmount = ((double.parse(previousRiderEarnings)) + (double.parse(perParcelDeliveryAmount))).toString();
    FirebaseFirestore.instance
        .collection("orders")
        .doc(getOrderId).update({
      "status": "ended",
      "adress": completeAdress,
      "lat": position!.latitude,
      "lng": position!.longitude,
      "earnings": perParcelDeliveryAmount, //pay per parcel delivery amount
    }).then((value)
    {
      FirebaseFirestore.instance
          .collection("riders")
          .doc(sharedPreferences!.getString("uid"))
          .update(
          {
            "earnings": riderNewTotalEarningAmount, //total earnings amount of rider
          });
    }).then((value)
    {
      FirebaseFirestore.instance
          .collection("sellers")
          .doc(widget.sellerId)
          .update(
          {
            "earnings": (double.parse(orderTotalAmount) + (double.parse(previousSellerEarnings))).toString(), //total earnings amount of seller
          });
    }).then((value)
    {
      FirebaseFirestore.instance
          .collection("users")
          .doc(purchaserId)
          .collection("orders")
          .doc(getOrderId).update(
          {
            "status": "ended",
            "riderUID": sharedPreferences!.getString("uid"),
          });
    }).then((value) {
      Navigator.push(context, MaterialPageRoute(builder: (c)=> const HomeScreen()));
    });
  }

  getOrderTotalAmount()
  {
    FirebaseFirestore.instance
        .collection("orders")
        .doc(widget.getOrderId)
        .get()
        .then((snap)
    {
      orderTotalAmount = snap.data()!["totalAmount"].toString();
      widget.sellerId = snap.data()!["sellerUID"].toString();
    }).then((value)
    {
      getSellerData();
    });
  }
  getSellerData()
  {
    FirebaseFirestore.instance
        .collection("sellers")
        .doc(widget.sellerId)
        .get().then((snap)
    {
      previousSellerEarnings = snap.data()!["earnings"];//variable globalda
    });
  }

@override
  void initState() {
    // TODO: implement initState
    super.initState();

    RiderLocation rLocation = RiderLocation();
    rLocation.getCurrentLocation();

    getOrderTotalAmount();
  }



  @override
  Widget build(BuildContext context) {
    return SafeArea(
            child: WillPopScope(
              onWillPop: ()async =>false,
              child: Scaffold(
                appBar: SimpleAppBar(title: LocaleKeys.Parcel_is_being_delivered.tr(),),
        body: Column(
          children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 30, 10, 0),
                child: Container(
                    width:MediaQuery.of(context).size.width-20,
                    height: MediaQuery.of(context).size.height*0.35,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage("images/confirm2.jpg"),fit: BoxFit.cover),
                      border: Border.all(color: Colors.grey),),),
              ),
              const SizedBox(height: 5,),

              GestureDetector(
                onTap: ()
                {
                  //show location from rider current location towards seller location
                  MapUtils.lauchMapFromSourceToDestination(position!.latitude, position!.longitude, widget.purchaserLat, widget.purchaserLng);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    Image.asset(
                      'images/restaurant.png',
                      width: 50,
                    ),

                    const SizedBox(width: 7,),

                    Column(
                      children:  [
                        SizedBox(height: 12,),
                        StyledTextWidget(text: LocaleKeys.Show_Delivery_Drop_off_Location.tr(), maxLines: 3,),
                      ],
                    ),

                  ],
                ),
              ),


              Padding(
                padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                child: Center(
                  child: InkWell(
                    onTap: ()
                    {

                      //rider location update
                      RiderLocation rLocation = RiderLocation();
                      rLocation.getCurrentLocation();

                      //confirmed - that rider has picked parcel from seller
                      confirmParcelHasBeenDelivered(
                          widget.getOrderId,
                          widget.sellerId,
                          widget.purchaserId,
                          widget.purchaserAdress,
                          widget.purchaserLat,
                          widget.purchaserLng
                      );
                      Navigator.push(context, MaterialPageRoute(builder: (c)=> const HomeScreen()));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(240, 86, 52, 1),
                        borderRadius: BorderRadius.circular(20),),
                      width: MediaQuery.of(context).size.width - 70,
                      height: 70,
                      child: Center(
                        child: StyledTextWidget(text: LocaleKeys.Order_has_been_Delivered.tr(),size: 15,color: Colors.white,maxLines: 3,),
                      ),
                    ),
                  ),
                ),
              ),

          ],
        ),
      ),
            ),
    );
  }
}
