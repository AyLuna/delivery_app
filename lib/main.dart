import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../authentication/auth_screen.dart';
import '../mainScreens/home_screen.dart';
import 'generated/codegen_loader.g.dart';
import 'global/global.dart';

Future<void> main() async
{
  WidgetsFlutterBinding.ensureInitialized();

  sharedPreferences = await SharedPreferences.getInstance();

  await EasyLocalization.ensureInitialized();

  await Firebase.initializeApp();

  runApp(
      EasyLocalization(
          supportedLocales: [Locale('en'), Locale('ru'), Locale('tr')],
          path: 'assets/translations', // <-- change the path of the translation files
          fallbackLocale: Locale('en'),
          assetLoader: CodegenLoader(),
          child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: (firebaseAuth.currentUser!=null)?HomeScreen():AuthScreen(),
    );
  }
}





