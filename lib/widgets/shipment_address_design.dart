import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../assistendMethods/get_current_location.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../mainScreens/home_screen.dart';
import '../mainScreens/parcel_picking_screen.dart';
import '../modal/adress.dart';
import 'styled_text_widget.dart';

class ShipmentAddressDesign extends StatelessWidget
{
  final Adress? model;
  final String? orderStatus;
  final String? orderId;
  final String? sellerId;
  final String? orderByUser;

  const ShipmentAddressDesign({Key? key, this.model, this.orderStatus, this.orderId, this.sellerId, this.orderByUser}) : super(key: key);

  confirmedParcelShipment(BuildContext context, String getOrderID, String sellerId, String purchaserId)
  {
    FirebaseFirestore.instance
        .collection("orders")
        .doc(getOrderID)
        .update({
      "riderUID": sharedPreferences!.getString("uid"),
      "riderName": sharedPreferences!.getString("name"),
      "status": "picking",
      "lat": position!.latitude,
      "lng": position!.longitude,
      "address": completeAdress,
    });

    //send rider to shipmentScreen
    Navigator.push(context, MaterialPageRoute(builder: (context) => ParcelPickingScreen(
      purchaserId: purchaserId,
      purchaserAdress: model!.fullAdress,
      purchaserLat: model!.lat,
      purchaserLng: model!.lng,
      sellerId: sellerId,
      getOrderId: getOrderID,
    )));
  }

  @override
  Widget build(BuildContext context)
  {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(10.0),
          child: StyledTextWidget(text: LocaleKeys.Shipping_Details.tr(),size: 18, weight:FontWeight.bold),//StyledTextWidget(text: "Shipping Details:",size: 18, weight:FontWeight.bold, color:Color.fromRGBO(240, 86, 52, 1)),
        ),
        const SizedBox(
          height: 6.0,
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 5),
          width: MediaQuery.of(context).size.width-10,
          child: Table(
            children: [
              TableRow(
                children: [
                  StyledTextWidget(text: LocaleKeys.Name.tr()),
                  StyledTextWidget(text: model!.name!),
                ],
              ),
              TableRow(
                children: [
                  StyledTextWidget(text: LocaleKeys.Phone.tr(),maxLines: 2,),
                  StyledTextWidget(text: model!.phoneNumber!),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: StyledTextWidget(text: model!.fullAdress!),

        ),

        orderStatus == "ended"
            ? Container()
            : Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: InkWell(
              onTap: ()
              {
                RiderLocation rLocation = RiderLocation();
                rLocation.getCurrentLocation();

                confirmedParcelShipment(context, orderId!, sellerId!, orderByUser!);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.cyan,
                  borderRadius: BorderRadius.circular(20),),
                width: MediaQuery.of(context).size.width - 70,
                height: 70,
                child: Center(
                  child: StyledTextWidget(text: LocaleKeys.Confirm_To_Deliver_this_Parcel.tr(),color: Colors.white,),
                ),
              ),
            ),
          ),
        ),


        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: InkWell(
              onTap: ()
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const HomeScreen()));
              },
              child: Container(
  decoration: BoxDecoration(
  color: Color.fromRGBO(240, 86, 52, 1),
  borderRadius: BorderRadius.circular(20),),
                width: MediaQuery.of(context).size.width - 70,
                height: 70,
                child: Center(
                  child: StyledTextWidget(text: LocaleKeys.Go_Back.tr(), color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 20,),
      ],
    );
  }
}
