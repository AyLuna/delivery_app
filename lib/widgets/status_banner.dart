import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/home_screen.dart';
import 'styled_text_widget.dart';


class StatusBanner extends StatelessWidget
{
  final bool? status;
  final String? orderStatus;

  StatusBanner({this.status, this.orderStatus});

  @override
  Widget build(BuildContext context)
  {
    String? message;
    IconData? iconData;

    status! ? iconData = Icons.done : iconData = Icons.cancel;
    status! ? message = LocaleKeys.Successful.tr() : message = LocaleKeys.Unsuccessful.tr();

    return Container(
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            onTap: ()
            {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const HomeScreen()));
            },
            child: const Icon(
              Icons.arrow_back_ios_new,size: 20,
              color: Colors.black,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          StyledTextWidget(text: orderStatus == "ended"
                          ? LocaleKeys.Parcel_Delivered.tr() +"$message"
                          : LocaleKeys.Order_Placed.tr() +"$message", size: 20,),
          const SizedBox(
            width: 5,
          ),
          CircleAvatar(
            radius: 10,
            backgroundColor: Colors.grey,
            child: Center(
              child: Icon(
                iconData,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
