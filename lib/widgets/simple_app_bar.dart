import 'package:dastawka/mainScreens/home_screen.dart';
import 'package:flutter/material.dart';

import 'styled_text_widget.dart';


class SimpleAppBar extends StatelessWidget with PreferredSizeWidget
{
  String? title;
  final PreferredSizeWidget? bottom;

  SimpleAppBar({this.bottom, this.title});

  @override
  Size get preferredSize => bottom==null?Size(56, AppBar().preferredSize.height):Size(56, 100+AppBar().preferredSize.height);

  @override
  Widget build(BuildContext context)
  {
    return AppBar(
      iconTheme: IconThemeData(
          color:Color.fromRGBO(72, 72, 72, 1), size: 25
      ),
      leading:IconButton(onPressed: (){
        Navigator.push(context, MaterialPageRoute(builder: (c)=>HomeScreen()));
      }, icon: Icon(Icons.arrow_back_ios_new)),
      flexibleSpace: Container(
        color: Colors.white,
      ),
      centerTitle: true,
      title: StyledTextWidget(text: title!,size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
    );
  }
}

