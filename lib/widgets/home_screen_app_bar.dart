import '../widgets/styled_text_widget.dart';
import '../global/global.dart';
import 'package:flutter/material.dart';

import 'language_picker.dart';
class HomeScreenAppBar extends StatefulWidget with PreferredSizeWidget{
  final PreferredSizeWidget? bottom;
  String? title;

  HomeScreenAppBar({Key? key, this.bottom, this.title}) : super(key: key);

  @override
  State<HomeScreenAppBar> createState() => _HomeScreenAppBarState();
  @override
  Size get preferredSize => bottom == null
      ? Size(56, AppBar().preferredSize.height)
      : Size(56, 80 + AppBar().preferredSize.height);
}

class _HomeScreenAppBarState extends State<HomeScreenAppBar> {
  final diller=["en","tm","ru"];
  String defaultDil="en";
  @override
  Widget build(BuildContext context) {
    return
      AppBar(
        iconTheme: IconThemeData(
            color:Color.fromRGBO(72, 72, 72, 1), size: 35
        ),
        automaticallyImplyLeading: false,
        flexibleSpace: Container(
          color: Colors.white,
        ),
        centerTitle: true,
        title: StyledTextWidget(text: "Bridge",size: 24,spacing: 3,weight:FontWeight.w500,color: Color.fromRGBO(240, 86, 52, 1),),
        actions: [
          LanguagePicker(),
        ],
      );
  }
}

