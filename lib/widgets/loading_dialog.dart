import 'package:easy_localization/easy_localization.dart';

import '../generated/locale_keys.g.dart';
import '../widgets/progress_bar.dart';
import 'package:flutter/material.dart';

import 'styled_text_widget.dart';
class LoadingDialog extends StatelessWidget {
  final String? message;

  const LoadingDialog({Key? key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      key: key,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          circularProgress(),
          SizedBox(height: 10,),
          StyledTextWidget(text: message! + LocaleKeys.Please_wait.tr(),),
        ],
      ),

    );
  }
}
